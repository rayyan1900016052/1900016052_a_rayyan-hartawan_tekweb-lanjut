import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
 selector: 'app-register',
 templateUrl: './register.component.html',
 styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
 //constructor
 constructor(
   public api:ApiService,
   public router:Router
 ) { }
 //fungsi inisial, dijalankan ketika class ini dipanggil
 ngOnInit(): void {
 }

user:any={};
hide:boolean=true
//form validation
email = new FormControl('', [Validators.required, Validators.email]);
password=new FormControl('',[Validators.minLength(6), Validators.required]);

loading:boolean | undefined;
 register(user: { email: any; password: any; })
 {
   this.loading=true;
   this.api.register(user.email, user.password).subscribe(res=>{
     this.loading=false;
     this.router.navigate(['auth/login']);
   },error=>{
     this.loading=false;
     alert('Ada masalah..');
   });
 }
}
